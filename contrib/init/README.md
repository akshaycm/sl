Sample configuration files for:

SystemD: stakinglabd.service
Upstart: stakinglabd.conf
OpenRC:  stakinglabd.openrc
         stakinglabd.openrcconf
CentOS:  stakinglabd.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
